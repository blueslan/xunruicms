# 迅睿CMS建站程序

迅睿CMS程序永久免费开源，自由二次开发的软件，属于所有PHPer及编程爱好者;
有任何开发建议都可以向作者提出，作者联系邮箱：finecms@qq.com

#### 运行环境
PHP7.2以上
MySQL5以上，推荐5.7+
Apache、Nginx、IIS都可以

#### 安装教程

1. 将代码下载到网站的根目录
2. 运行安装文件/install.php
3. 切忌不要放在子目录安装

#### 使用说明

1. 使用文档：https://www.xunruicms.com/doc/
2. 技术论坛：https://www.xunruicms.com/wenda/
3. 正式版程序下载：https://www.xunruicms.com/down/
